import React from "react";
import { NavLink, useNavigate } from "react-router-dom";
import styles from "./style.module.css";
import { Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { userServices } from "../../services/user.services";
import { loginAction } from "../../redux/actions/userAction";
import { localStorageService } from "../../services/localStorageService";

export default function LoginPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();

  const onFinish = (values) => {
    userServices
      .postLogin(values)
      .then((res) => {
        // gửi action về store
        dispatch(loginAction(res.data.content));
        // lưu dữ liệu vào localStorage
        localStorageService.user.set(res.data.content);

        message.success("Đăng nhập thành công");
        navigate("/");
      })
      .catch((err) => {
        message.error("Tài khoản hoặc mật khẩu không chính xác!")
      });
  };

  const onFinishFailed = (errorInfo) => {};

  return (
    <div className={styles.session}>
      <div className={styles.container}>
        <div className={styles.column_left}>
          <div className={styles.login_left}>
            <h2 className="text-white">Welcome Back</h2>
            <p>Create your account.</p>
            <NavLink to="/sign-up">
              <button>Sign up</button>
            </NavLink>
          </div>
        </div>
        <div className={styles.colum_right}>
          <div className={styles.login_form}>
            <h2>Login</h2>
            <Form
              name="basic"
              layout="vertical"
              labelCol={{
                span: 24,
              }}
              wrapperCol={{
                span: 24,
              }}
              initialValues={{
                remember: false,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label={<p className="font-medium">Username</p>}
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập tài khoản",
                  },
                ]}
              >
                <Input className="rounded py-2" placeholder="Username" />
              </Form.Item>

              <Form.Item
                label={<p className="font-medium">Password</p>}
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập mật khẩu",
                  },
                ]}
              >
                <Input.Password
                  className="rounded py-2"
                  placeholder="Password"
                />
              </Form.Item>

              <div className="flex justify-center">
                <button className="rounded px-5 py-2 text-white bg-red-500">
                  {" "}
                  Sign In
                </button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
