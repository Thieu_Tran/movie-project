import "../DetailPage/detailPage.css";
import { Modal, Rate } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import YouTube from "react-youtube";
import { movieService } from "../../services/movie.services";
import Footer from "../../components/Footer/Footer";
import Comments from "./Comments";

export default function DetailPage() {
  let { id } = useParams();
  const [movie, setMovie] = useState({});
  const [modal2Open, setModal2Open] = useState(false);

  const opts = {
    height: "390",
    width: "100%",
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 1,
    },
  };
  const _onReady = (event) => {
    // access to player in all event handlers via event.target
    event.target.mute();
  };

  useEffect(() => {
    movieService
      .getMovieDetail(id)
      .then((res) => {
        setMovie(res.data.content);
      })
      .catch((err) => {});
  });
  return (
    <div
      className="text-center px-2 py-5 border shadow min-h-screen relative"
      style={{ margin: "0 192px" }}
    >
      <div className="w-full text-2xl font-semibold text-red-500 py-2 mt-16 mb-5">
        {movie.tenPhim}
      </div>
      <div className="h-96 flex px-3">
        <div className="w-60 h-96 mr-5 px-2 py-2 shrink-0 shadow xl:w-80">
          <img
            src={movie.hinhAnh}
            alt=""
            className="w-full h-full object-cover rounded"
          />
        </div>
        <div className="grow text-left px-2 py-2 shadow hidden lg:block">
          <div className="w-full flex">
            <p className="w-28 py-2 shrink-0 font-semibold hidden xl:block">
              NỘI DUNG:{" "}
            </p>
            <p className="py-2 grow hidden xl:block">{movie.moTa}</p>
          </div>
          <div className="w-full flex">
            <p className="w-28 py-2 shrink-0 font-semibold">KHỞI CHIẾU: </p>
            <p className=" py-2 grow">
              {moment(movie.ngayKhoiChieu).format("dddd, DD-MM-YYYY")}
            </p>
          </div>
          <div className="w-full flex">
            <p className="w-28 py-2 shrink-0 font-semibold">ĐÁNH GIÁ: </p>
            <p className="py-2 grow">
              <Rate
                className=" text-sm"
                allowHalf
                value={(movie.danhGia * 5) / 10}
              />
            </p>
          </div>
          <div className="w-full flex">
            <p className="w-28 py-2 shrink-0 font-semibold">TRAILER: </p>
            <button
              className="py-2 px-5 bg-red-500 rounded text-white hover:bg-red-400 transition"
              onClick={() => setModal2Open(true)}
            >
              Click Here
            </button>
          </div>
          <Modal
            title={
              <p className="text-red-500 capitalize">
                {movie.tenPhim} (trailer)
              </p>
            }
            centered
            width={800}
            footer={null}
            open={modal2Open}
            onOk={() => setModal2Open(false)}
            onCancel={() => setModal2Open(false)}
          >
            <YouTube
              videoId={`${movie.trailer}`.slice(-11)}
              opts={opts}
              onReady={_onReady}
            />
          </Modal>
        </div>
      </div>
      <div className="w-full mx-3 my-5 text-left bg-bgColor">
        <span
          id="clipPath_content"
          className="w-36 text-white bg-red-500 flex items-center pl-2 py-2 text-md font-bold"
        >
          COMMENTS:
        </span>
      </div>
      <div className="mx-3 my-2 px-5 py-2 shadow text-left">
        <Comments />
      </div>
      <div className="w-full absolute bottom-0 left-0">
        <Footer />
      </div>
    </div>
  );
}
