import React from "react";
import Banner from "../../components/Banner/Banner";
import Footer from "../../components/Footer/Footer";
import MovieList from "../../components/MovieList/MovieList";
import TheaterSystem from "../../components/TheaterSystem/TheaterSystem";

export default function HomePage() {
  return (
    <div>
      <Banner />
      <MovieList/>
      <TheaterSystem />
      <Footer/>
    </div>
  );
}
