import { Form, Input, message } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";
import { userServices } from "../../services/user.services";
import styles from "./style.module.css";

export default function SignupPage() {
  const onFinish = (values) => {
    userServices
      .postSignup(values)
      .then((res) => {
        document.getElementById("reset-form").reset()
        message.success('Đăng ký tài khoản thành công')
      })
      .catch((err) => {
        message.error('Đăng ký tài khoản thất bại!')
      });
  };

  const onFinishFailed = (errorInfo) => {
  };

  return (
    <div className={styles.session}>
      <div className={styles.container}>
        <div className={styles.column_left}>
          <div className={styles.signup_form}>
            <h2>Sign Up</h2>

            <Form
            id="reset-form"
              name="basic"
              layout="vertical"
              labelCol={{
                span: 24,
              }}
              wrapperCol={{
                span: 24,
              }}
              initialValues={{
                remember: false,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label={<p className="font-medium">Fullname</p>}
                name="hoTen"
                rules={[
                  {
                    required: true,
                    message: "Please input your name",
                  },
                ]}
              >
                <Input className="rounded py-2" placeholder="Fullname" />
              </Form.Item>

              <Form.Item
                label={<p className="font-medium">Username</p>}
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: "Please input your username",
                  },
                ]}
              >
                <Input className="rounded py-2" placeholder="Username" />
              </Form.Item>

              <Form.Item
                label={<p className="font-medium">Password</p>}
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Please input your password",
                  },
                ]}
              >
                <Input.Password
                  className="rounded py-2"
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item
                label={<p className="font-medium">Email</p>}
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Please input your email",
                  },
                ]}
              >
                <Input className="rounded py-2" placeholder="Email" />
              </Form.Item>
              <Form.Item
                label={<p className="font-medium">Phone number</p>}
                name="soDt"
                rules={[
                  {
                    required: true,
                    message: "Please input your phone number",
                  },
                ]}
              >
                <Input className="rounded py-2" placeholder="Phone number" />
              </Form.Item>
              <Form.Item className="hidden"
                label={<p className="font-medium">Group Code</p>}
                name="maNhom"
                initialValue='GP03'
                rules={[
                  {
                    required: false,
                    message: "Please input your group code",
                  },
                ]}
              >
                <Input className="rounded py-2"/>
              </Form.Item>

              <div className="flex justify-center">
                <button className="rounded px-5 py-2 text-white bg-red-500">
                  {" "}
                  Sign Up
                </button>
              </div>
            </Form>
          </div>
        </div>
        <div className={styles.column_right}>
          <div className={styles.signup_right}>
            <h2 className="text-white">Welcome Back</h2>
            <p>Already have an account ?</p>
            <NavLink to="/login">
              <button>Login</button>
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}
