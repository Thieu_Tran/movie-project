import { https } from "./configURL";

export let userServices = {
  postLogin: (loginData) => {
    return https.post("/api/QuanLyNguoiDung/DangNhap", loginData);
  },
  postSignup:(signupData) => { 
    return https.post('/api/QuanLyNguoiDung/DangKy',signupData)
   }
};
