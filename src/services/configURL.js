import axios from "axios";

export const TOKEN_MOVIE =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJKYXZhIDIxIiwiSGV0SGFuU3RyaW5nIjoiMjkvMTEvMjAyMyIsIkhldEhhblRpbWUiOiIxNzAxMjE2MDAwMDAwIiwibmJmIjoxNjc4NzI2ODAwLCJleHAiOjE3MDEzNjM2MDB9.0HYvOh58GW4sd0002lHfUc6rCdx6G_3R-Nh9X8vVSkA";

export let https = axios.create({
  baseURL: "https://movienew.cybersoft.edu.vn",
  headers: {
    TokenCybersoft: TOKEN_MOVIE,
  },
});
