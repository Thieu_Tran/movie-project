const USER = "USER";

export let localStorageService = {
  user: {
    set: (userData) => {
      let dataJson = JSON.stringify(userData);
      localStorage.setItem(USER, dataJson);
    },
    get: () => {
      let dataJson = localStorage.getItem(USER);
      if (!dataJson) {
        return null;
      } else {
        return JSON.parse(dataJson);
      }
    },
    remove: () => {
      localStorage.removeItem(USER);
    },
  },
};
