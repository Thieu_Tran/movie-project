import { combineReducers } from "redux";
import { movieReducer } from "./movieReuducer";
import { userReducer } from "./userReducer";


export let rootReducer = combineReducers({
    userReducer,
    movieReducer,
})