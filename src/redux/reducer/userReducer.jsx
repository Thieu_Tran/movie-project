import { localStorageService } from "../../services/localStorageService"
import { LOGIN } from "../constants/userConstant"

let initialState = {
    userInfor:localStorageService.user.get(),
}

export let userReducer = (state = initialState,action)=>{
    switch(action.type){
        case LOGIN:{
            state.userInfor = action.payload;
            return {...state}
        }
        default:
            return state
    }
}