import { MOVIE_FILTER, SET_MOVIE_LIST } from "../constants/movieConstants";

const initialState = {
  movieList: [],
  movieFilter: "",
};

export const movieReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_MOVIE_LIST:
      state.movieList = payload;
      return { ...state };
    case MOVIE_FILTER:
      state.movieFilter = payload;
      return { ...state };
    default:
      return state;
  }
};
