import { movieService } from "../../services/movie.services";
import {SET_MOVIE_LIST } from "../constants/movieConstants";

export const movieListActionService = () => {
  return (dispatch) => {
    movieService
      .getMovieList()
      .then((res) => {
        dispatch({
          type: SET_MOVIE_LIST,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

