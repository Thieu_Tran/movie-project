import { Card, Pagination, Rate } from "antd";
import Meta from "antd/lib/card/Meta";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { removeVietnameseTones } from "../../functions/removeVietnamese";
import { movieListActionService } from "../../redux/actions/movieAction";
import "../MovieList/movieList.css";
import { motion } from "framer-motion";

export default function MovieList() {
  let dispatch = useDispatch();
  const [current, setCurrent] = useState(1);
  useEffect(() => {
    dispatch(movieListActionService());
  }, []);

  let { movieList, movieFilter } = useSelector((state) => {
    return state.movieReducer;
  });
  const onChange = (page) => {
    setCurrent(page);
  };

  // phân trang cho movie item
  const moviePerPage = 15;
  const pagesVisited = moviePerPage * (current - 1);
  // map dữ liệu kết hợp với filter, includes
  let renderMovieList = () => {
    return movieList
      .filter((item) => {
        return removeVietnameseTones(item.tenPhim)
          .toLowerCase()
          .includes(removeVietnameseTones(movieFilter).toLowerCase());
      })
      .slice(pagesVisited, pagesVisited + moviePerPage)
      .map((movie, index) => {
        return (
          <NavLink to={`detail/${movie.maPhim}`} key={index}>
            <div
              id="card_movie"
              className="w-full px-2 py-2 border border-bgColor relative z-10 hover:border-red-500"
              style={{ height: "400px" }}
            >
              <Card
                className="w-full h-full overflow-hidden rounded-lg drop-shadow-lg"
                hoverable
                cover={
                  <img
                    alt="example"
                    src={movie.hinhAnh}
                    className="w-full h-80 object-cover"
                  />
                }
              >
                <div className="w-full h-full">
                  <Meta
                    className="justify-center"
                    title={
                      <p className="text-bgColor capitalize truncate">
                        {movie.tenPhim}
                      </p>
                    }
                  />
                  <Rate
                    className=" text-sm"
                    allowHalf
                    value={(movie.danhGia * 5) / 10}
                  />
                </div>
              </Card>
              <div id="card_overlay">
                <button>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className=" text-bgColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                    />
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M15.91 11.672a.375.375 0 010 .656l-5.603 3.113a.375.375 0 01-.557-.328V8.887c0-.286.307-.466.557-.327l5.603 3.112z"
                    />
                  </svg>
                </button>
              </div>
            </div>
          </NavLink>
        );
      });
  };

  return (
    <div id="card_movieList">
      <div className="w-full text-left bg-bgColor">
        <span
          id="clipPath_content"
          className="w-36 text-white bg-red-500 flex items-center pl-2 py-2 text-md font-bold"
        >
          MOVIE LIST
        </span>
      </div>
      <motion.div layout className="w-full grid 2xl:grid-cols-5 xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 grid-cols-1">
        {renderMovieList()}
      </motion.div>
      <Pagination onChange={onChange} total={Math.ceil(movieList.length*10/15)} className="my-3" />
    </div>
  );
}
