import React from "react";
import MapTheaterItem from "./MapTheaterItem";

export default function TheaterSystem() {
  return (
    <div id="cinemaSystem" className="container mx-auto">
      <div className="w-full text-left bg-bgColor">
        <span
          id="clipPath_content"
          className=" text-white bg-red-500 flex items-center pl-2 py-2 w-36 text-md font-bold"
        >
          CINEMA SYSTEM
        </span>
      </div>
      <div className="px-2 py-5">
        <MapTheaterItem />
      </div>
    </div>
  );
}
