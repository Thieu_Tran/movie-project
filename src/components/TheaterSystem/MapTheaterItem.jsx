import "./theaterSystem.css";
import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { movieService } from "../../services/movie.services";
import moment from "moment";
const { TabPane } = Tabs;

export default function MapTheaterItem() {
  const [dataMovie, setDataMovie] = useState([]);

  useEffect(() => {
    movieService
      .getMovieTheater()
      .then((res) => {
        setDataMovie(res.data.content);
      })
      .catch((err) => {});
  }, []);

  const renderTheaterSystem = () => {
    return dataMovie.map((heThongRap, index) => {
      return (
        <TabPane
          tab={<img src={heThongRap.logo} className="w-14 h-14" />}
          key={index}
        >
          <Tabs
            style={{ height: "432px" }}
            tabPosition="left"
            defaultActiveKey="1"
          >
            {heThongRap.lstCumRap.map((cumRap) => {
              return (
                <TabPane tab={renderCumRap(cumRap)} key={cumRap.maCumRap}>
                  <Tabs
                    style={{ height: "432px" }}
                    tabPosition="left"
                    defaultActiveKey="1"
                  >
                    {cumRap.danhSachPhim
                      .filter((phim) => {
                        return phim.hinhAnh != null;
                      })
                      .map((phim) => {
                        return (
                          <TabPane
                            tab={renderLichChieu(phim)}
                            key={phim.maPhim}
                          ></TabPane>
                        );
                      })}
                  </Tabs>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };

  const renderCumRap = (cumRap) => {
    return (
      <div className="w-16 lg:w-80 xl:w-60 flex items-center hover:bg-slate-100 2xl:w-96">
        <div className="w-14 h-14 shrink-0">
          <img
            src={cumRap.hinhAnh}
            alt=""
            className="w-full h-full object-cover"
          />
        </div>
        <div className="text-left truncate ml-5">
          <p className="truncate">{cumRap.tenCumRap}</p>
          <p className="truncate">{cumRap.diaChi}</p>
        </div>
      </div>
    );
  };

  const renderLichChieu = (phim) => {
    return (
      <div className="w-16 flex items-center hover:bg-slate-100 xl:w-max">
        <div className="w-14 h-14 shrink-0">
          <img
            src={phim.hinhAnh}
            alt=""
            className="w-full h-full object-cover"
          />
        </div>
        <div className="text-left truncate ml-5">
          <p className="truncate">{phim.tenPhim}</p>
          <div className="grid grid-cols-4 gap-2">
            {phim.lstLichChieuTheoPhim.slice(0, 4).map((lichChieu) => {
              return (
                <div
                  key={lichChieu.maLichChieu}
                  className=" bg-red-500 text-white p-1 rounded"
                >
                  {moment(lichChieu.ngayChieuGioChieu).format("DD-MM")}

                  <span className="text-yellow-400 font-bold text-base ml-2">
                    {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
                  </span>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="container mx-auto">
      <Tabs style={{ height: "432px" }} tabPosition="left" defaultActiveKey="1">
        {renderTheaterSystem()}
      </Tabs>
    </div>
  );
}
