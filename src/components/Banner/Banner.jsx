import "../Banner/banner.css";
import React from "react";
import Slider from "react-slick";

export default function Banner() {

  var settings = {
    dots: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipeToSlide: true,
    adaptiveHeight:false,
    centerPadding:0,
  };
  return (
    <div className="banner">
      <Slider {...settings} className='w-full'>
        <div className="w-full" >
          <img
            src='https://boophim.com/media/tm/lXhgCODAbBXL5buk9yEmTpOoOgR.jpg'
            alt=""
            className="w-full max-h-max object-cover"
          />
        </div>
        <div className="w-full">
          <img
            src='https://motgame.vn/wp-content/uploads/2022/04/hogwarts-thumb.jpg'
            alt=""
            className="w-full object-cover"
          />
        </div>
        <div className="w-full">
          <img
            src='https://www.gamebyte.com/wp-content/uploads/2021/11/ef65e34f-marvels-avengers-spider-man-3.jpeg'
            alt=""
            className="w-full object-cover"
          />
        </div>
      </Slider>
    </div>
  );
}
