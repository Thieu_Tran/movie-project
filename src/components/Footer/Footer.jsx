import React from "react";

export default function Footer() {
  return (
    <div>
      <div className="container mx-auto pb-3 pt-2 mb-0 px-5 bg-bgColor">
        <div className="flex items-center justify-center mx-48">
            <p className="text-white font-semibold">Copyright <i className="fa fa-copyright"></i> 2022 All Rights Reserved</p>
        </div>
      </div>
    </div>
  );
}
