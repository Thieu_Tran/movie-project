import "./hotMovie.css";
import React, { useEffect } from "react";
import Slider from "react-slick";
import { useDispatch, useSelector } from "react-redux";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {
  movieListActionService,
} from "../../redux/actions/movieAction";
import { Card, Rate } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function HotMovie() {
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(movieListActionService());
  },[]);
  let { movieList } = useSelector((state) => {
    return state.movieReducer;
  });
  var settings = {
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 4,
    slidesToScroll: 1,
    vertical: true,
    verticalSwiping: true,
  };
  return (
    <div className="flex fixed top-0 right-0 bottom-0 w-48 h-screen items-center justify-center bg-bgColor border-l z-50">
      <Slider {...settings} className="w-48">
        {movieList
          .filter((item) => {
            return item.hot == true;
          })
          .map((item, index) => {
            return (
              <NavLink to={`/detail/${item.maPhim}`} key={index}>
                <div
                  id="card_movie"
                  className="w-full h-72 px-2 py-2 bg-bgColor border-none relative z-20"
                >
                  <Card
                    className="w-full h-full relative"
                    hoverable
                    cover={
                      <img
                        alt="example"
                        src={item.hinhAnh}
                        className="w-full h-52 object-cover"
                      />
                    }
                  >
                    <span className="absolute top-0 left-0 text-white bg-red-500 rounded px-2 text-md font-semibold">Hot</span>
                    <div className="w-full h-full">
                      <Meta
                        className="justify-center"
                        title={
                          <p className="text-bgColor capitalize truncate">
                            {item.tenPhim}
                          </p>
                        }
                      />
                      <Rate
                        className=" text-sm"
                        allowHalf
                        value={(item.danhGia * 5) / 10}
                      />
                    </div>
                  </Card>
                  <div id="card_overlay">
                    <button>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-8 h-8 text-bgColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                        />
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M15.91 11.672a.375.375 0 010 .656l-5.603 3.113a.375.375 0 01-.557-.328V8.887c0-.286.307-.466.557-.327l5.603 3.112z"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
              </NavLink>
            );
          })}
      </Slider>
    </div>
  );
}
