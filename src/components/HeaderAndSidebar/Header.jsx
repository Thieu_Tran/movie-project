import React, { useRef } from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { MOVIE_FILTER } from "../../redux/constants/movieConstants";

export default function Header() {
  let dispatch = useDispatch();
  const typingTimeoutRef = useRef(null);


  const handleChangeFilter = (e) => {
    if(typingTimeoutRef.current){
      clearTimeout(typingTimeoutRef.current)
    }
    typingTimeoutRef.current = setTimeout(() => { 
      dispatch({ type: MOVIE_FILTER, payload: e.target.value });
     },300)
  };

  return (
    <div
      className="mx-auto pb-3 pt-2 mb-0 px-5 bg-bgColor fixed z-20"
      style={{ width: "100%" }}
    >
      <form className="flex mx-48">
        <label htmlFor="simple-search" className="sr-only">
          Search
        </label>
        <div className="relative w-full flex justify-between items-center space-x-5 md:space-x-10">
          <NavLink to={"/"}>
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-8 h-8 text-white hover:cursor-pointer hover:text-red-500 duration-200"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25"
                />
              </svg>
            </div>
          </NavLink>

          <div className="flex absolute inset-y-0 right-4 items-center pl-3 pointer-events-none">
            <svg
              aria-hidden="true"
              className="w-5 h-5 text-gray-500 dark:text-gray-400"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <input
            onChange={handleChangeFilter}
            type="text"
            id="simple-search"
            className="w-12 h-12 rounded-full hover:w-full focus:w-full duration-300 bg-gray-50 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="Search..."
            required
          />
        </div>
      </form>
    </div>
  );
}
