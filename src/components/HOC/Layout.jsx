import React from "react";
import Header from "../HeaderAndSidebar/Header";
import SideBar from "../HeaderAndSidebar/SideBar/SideBar";
import HotMovie from "../HotMovie/HotMovie";

export default function Layout({ Component }) {
  return (
    <div>
      <SideBar />
      <Header/>
      <HotMovie/>
      <Component />
    </div>
  );
}
